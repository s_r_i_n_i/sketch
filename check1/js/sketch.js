//=========================================================== SKETCH_CLASS ============================================================================================//

function sketch_class(){
	
		//=============================== ONLOADING_CONTENT ====================================================//
			  this.flag = 0;
    
			this.onload = function(){
				
				 var x = JSON.parse(gsession.getSession('json_data'));
                console.log(x);
                if(x)
                    {sketchpad.reanimate(x.strokes,0);}
                 
                
                 var t=sketchpad.penSize;
                 document.getElementById("thick").textContent=t; 
                
                 for (var i=0; i<360; i++) {
                 var color = document.createElement("span")
                 color.setAttribute("id", "c" + i)
                 color.style.backgroundColor = "hsl(" + i + ", 100%, 50%)"
                 color.style.msTransform = "rotate(" + i + "deg)"
                 color.style.webkitTransform = "rotate(" + i + "deg)"
                 color.style.MozTransform = "rotate(" + i + "deg)"
                 color.style.OTransform = "rotate(" + i + "deg)"
                 color.style.transform = "rotate(" + i + "deg)"
                 document.getElementById('colorwheel').appendChild(color)
                 }
                
                 $( "#draggable" ).draggable();
                
                 $('.ui.dropdown').dropdown();
				}
             
            
			this.increment = function(){
                this.flag++;
                return this.flag;
            }	
            
            
}
		
//================================================================= END OF SKETCH CLASS
//=================================================================================================//
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
//================================================================= DOCUMENT READY FUNCTION =================================================================================================//
$(document).ready(function() {
	
		var sketch_obj = new sketch_class();
		     var x= 4000;
             var y= 4000;
             var sketchpad = new Sketchpad({
             element: '#sketchpad',
             width: x,
             height: y,
             penSize:'3',
             });
             sketchpad.penSize=2;
             sketchpad.color="#333333";
	
		
		//=================== ONLOAD ==========================//
		
		    sketch_obj.onload();
		
		
		//================================ MAKE A COPY ============================//
		
			$('#make_copy').on('click',function c(){
            var my_data = {};
            
            my_data['json_data'] = gsession.getSession('json_data');
            console.log(my_data.json_data);
            $('.ui.basic.modal.modal4').modal('show');
            sketch_obj.increment();
            
            });	
		
	
		//===================================== RENAME SKETCHES =============================//
		
			$('#rename').on('click', function r(){
            $('.ui.basic.modal.modal3').modal('show');
            var my_data = {};
            my_data['json_data'] = sketchpad.toJSON();
            sketch_obj.increment();
            });
    
            $("#submit1").on('click', function(data){
            var my_data = {};
            var sketch_name = document.getElementById("content1").value;
            gsession.setSession('name',sketch_name);
            my_data['active_sketch'] = gsession.getSession('active_sketch');
            my_data['name'] = sketch_name;
            my_data['action'] = "rename_sketch";
            ajaxcall.send_data(my_data,'sketch_insert',function(data) {
            response_data = data; 
            });
            var active_sketch = JSON.parse(response_data).data;
            gsession.setSession('active_sketch',active_sketch);
            $('.ui.basic.modal.modal3').modal('hide');
            sketch_obj.increment();
            });
		
		
		//=================================== ANIMATE SKETCHES =============================//
		
			$('#animate').on('click',function f(){
            var count = sketch_obj.flag;
            if(count==0)
                    sketchpad.animate1(10);
            else 
                    sketchpad.animate(10);
            });
		
		//=================================== UNDO SKETCHES =============================//
            $('#clear').on('click', function c(){
            var count = sketch_obj.flag;
            if(count==0)
                    sketchpad.undo1();
            else 
            //sketch_obj.increment();
            sketchpad.undo();
            });
        
        //================================== REDO SKETCHES =================================//
    
            $('#redo').on('click', function c(){
            sketchpad.redo();
            sketch_obj.increment();
            });
    
        //================================= CLEAR SKETCHES =================================//
    
            $('#clearall').on('click', function cl(){
            sketchpad.clearSketch();
            gsession.clearSession('json_data');
            sketch_obj.increment();
            });
    
        //==================================ADDING NEW SKETCHES============================//
    
            $('#new').on('click', function(){
            var my_data={};
            gsession.clearSession('active_sketch');
            $('.ui.basic.modal.modal1').modal('show');
            sketchpad.clearSketch();
            sketch_obj.increment();
            });
    
            $("#submit").on('click',function(data){
            var sketch_name = document.getElementById("content").value;
            gsession.setSession('name',sketch_name);
            var my_data={};
            my_data['name']=sketch_name;
            my_data['user_hash']=gsession.getSession('user_hash');
            my_data["sketch_data"] = sketchpad.toJSON();
            my_data["action"] = "insert_sketch";
            ajaxcall.send_data(my_data,'sketch_insert',function(data) {
            response_data = data; 
            });
            var active_sketch = JSON.parse(response_data).data;
            gsession.setSession('active_sketch',active_sketch);
            $('.ui.basic.modal.modal1').modal('hide');
            sketch_obj.increment();
            });
    
         //=================================OPEN SKETCHES=============================//
    
            $('#open').on('click', function(){
            var my_data={};
            my_data["user_hash"] = gsession.getSession('user_hash');
            my_data["sketch_name"] = gsession.getSession('name');
            my_data['action'] = "select_sketch";
            ajaxcall.send_data(my_data, 'sketch_insert', function(data) {
            var list = "";
            data = JSON.parse(data);
            $.each(data, function(i, item) {
                var sketch_id = item.data_id;
                var sketch_name = item.name;
                var timestamp = item.timestamp;
                var time = item.time;
                var fulldate = new Date(timestamp*1000);
                var date = "0"+fulldate.getDate();
                var month = "0"+fulldate.getMonth();
                var year = fulldate.getFullYear();
                list+="<li id="+sketch_id+" class='sketch_ids'><span class='name'>"+sketch_name+"</span><span class='date_time'>"+timestamp+"</span></li>";
            })
            document.getElementById("sketches").innerHTML=list;
            $('.sketch_ids').on('click',function(e){
            sketchpad.clearSketch();
            console.log(e);
            var sketch_id=e.currentTarget.id;
            var my_data={};
            my_data.data_id=sketch_id;
            my_data['action']="fetch_sketch";
            ajaxcall.send_data(my_data,'sketch_insert',function(data){
            data = JSON.parse(data);
            var sketch=JSON.parse(data.sketch_data);
            gsession.setSession('name',data.name);
            gsession.setSession('active_sketch',sketch_id);
            gsession.setSession('json_data',JSON.stringify(sketch));
            console.log(gsession.getSession('json_data'));
            $('.ui.basic.modal.modal2').modal('hide');
            console.log(sketch.strokes);
            sketchpad.reanimate(sketch.strokes,0);
            });
            });
            });
            $('.ui.basic.modal.modal2')
            .modal('show');
            var count = sketch_obj.increment();
            console.log(count);
            });
    
          //============================================Mouse-down and Mouse-up========================================================//
    
    
            $('canvas').on('mousedown mouseup', function mouseState(e) {
            var my_data = {};
            if (e.type == "mousedown") {
            console.log("hold");
            }
            else if(e.type == 'mouseup') {
            my_data["sketch_data"] = sketchpad.toJSON();
            my_data['active_sketch'] = gsession.getSession('active_sketch');
            gsession.setSession('json_data',my_data.sketch_data);
            my_data['action'] = "update_sketch";
            ajaxcall.send_data(my_data,'sketch_insert',function(data) {
            response_data = data; 
            });
            }
            });
    
    
          //===========================================Thickness of line==============================================================//
    
    
            $('#thickness').on('input',function(){
            sketchpad.penSize=parseInt(this.value);
            document.getElementById("thick").textContent=(this.value);
            });
        
          //=========================================== FAB ==========================================================================//
            
            $('.fab').click (function(){
            $('.radial').toggleClass('open');
            });
    
            $('.fab').on('click', function(){
            var div = document.getElementsByClassName('radial')[0];
            div.style.display = div.style.display == "none" ? "block" : "none";
            });
    
          
          //========================================== Sketchpad background color====================================================//
     
            $('span').on('click',function() {
            var v = document.getElementById(this.id);
            sketchpad.color = v.style.backgroundColor;
            document.getElementById('color').style.color=v.style.backgroundColor;
            });
    
    
    
         //======================================== Make a copy when onload =========================================================//
    
     
    
    
            $("#submit2").on('click',function(data){
            var sketch_name = document.getElementById("content2").value;
            gsession.setSession('name',sketch_name);
            var my_data={};
            my_data['name']=sketch_name;
            my_data['user_hash']=gsession.getSession('user_hash');
            my_data["sketch_data"] = gsession.getSession('json_data');
            my_data["action"] = "insert_sketch";
            ajaxcall.send_data(my_data,'sketch_insert',function(data) {
            response_data = data; 
            });
            var active_sketch = JSON.parse(response_data).data;
            gsession.setSession('active_sketch',active_sketch);
            $('.ui.basic.modal.modal4').modal('hide');
            sketch_obj.increment();
            });
});
//================================================================= END OF THE FUNCTION 




