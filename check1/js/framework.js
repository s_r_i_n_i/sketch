var logs = {};
var ajaxcall = {};
var response_logs = {};
/**
function to send logs to database
*/

/*   wrapper function to pass the data    */
ajaxcall.send_data = (function(json_value, url_id, callback) {

    var json_data = JSON.stringify(json_value);
    var mapping = url_mapper(url_id);
    $.ajax({
        type: "POST",
        async: false,
        data: {
            'myData': json_data
        },
        url: mapping
    }).done(function(result) {
        callback(result);
    });
});


/*

Function to return error message for error key

How to use ?

var response_text=response_logs.get_message("login_failed");
console.log(response_text);

*/

response_logs.get_message = (function(response_key,callback) {

    var error_log = {
        'login_success': "you have logged in successfully!!!",
        'login_failed': "login failed"
    };
    return error_log[response_key];

});

function url_mapper(page_id) {
    var pages = {
        'sketch_insert':"model/sketch_insert.php",
        'login': "model/login.php",
        'user_insert': "model/user_insert.php",
        'update_profile': "model/update_profile.php",
        'change_password': "model/change_password.php",
        'project_insert': window.location.pathname.includes('preview')?"../model/project_insert.php":"model/project_insert.php",
        'all_projects': "model/all_projects.php",
        'team_management': "model/team_management.php",
        'project_display': "model/view_project.php",
        'list_users': "model/list_users.php",
        'project_preview': "model/preview_project.php",
        'map': "model/map.php",
        'enquiry_handle': "model/enquiry_handle.php",
        'pincode': "model/pincode_assignment.php",
        'pincode1': "model/pincode_assignment1.php",
        'enquiry_list': "model/all_enquiries.php",
        'boq_add_details': "model/boq_add_details.php",
        'boq_prod_details': "model/boq_prod_details.php",
        'boq_filter': "model/boq_filter.php",
        'boq_add_filter_details': "model/boq_add_filter_details.php",
        'view_cart': "model/view_cart.php",
        'vendor_mapping': "model/vendor_mapping.php",
        'vendor_list': "model/vendor_list.php",
        'category_insert': "model/category_insert.php",
        'vendor_insert': "model/vendor_insert.php",
        'product_insert': "model/product_insert.php",
        'varient_insert': "model/varient_insert.php",
        'brand_insert': "model/brand_insert.php",
        'oauth_login': "model/login.php",
        'support_ticket': "model/support_ticket.php",
        'work_feed': "model/work_feed.php",
        'absence_management': "model/absence_management.php",
        'admin_page': "model/admin_dashboard.php",
        'personal_ticket': "model/personal_ticket.php",
        'project_fund': "model/expected_funds.php",
        'space_insert': "model/space_insert.php",
        'scope_insert': "model/scope_insert.php",
        'sub_scope_insert': "model/sub_scope_insert.php",
        'typology_insert': "model/typology_insert.php",
        'sub_typology_insert': "model/sub_typology_insert.php",
        'space_mapping': "model/space_mapping.php",
        'dq_intent': "model/dq_intent.php",
        'image_note': "model/image_notes.php",
        'topic_insert':"model/topic_details.php",
		'space_topic_mapping':"model/space_topic_mapping.php",
        'roles_mapping': "model/roles_mapping.php",
        'intent_dashboard': "model/intent_dashboard.php",
        'intent_image_upload': "model/intent_image_upload.php",
        'intent_idea_list': "model/intent_idea_list.php",
        'user_upload_image': "model/user_upload_image.php",
        'design_priorities': "model/design_priorities.php",
        'project_space_mapping': "model/project_space_mapping.php",
		'notification_select':"model/notification_select.php",
		'notification_read':"model/notification_views.php",
		'showallnotification':"model/showallnotification.php",
		'notification_unread':"model/notification_views.php",
		'email_template': "sendgrid-php/email_template.php",
		'invoice_email_template': "sendgrid-php/invoice_email_template.php",
        'create_enquiry': "dqapp/model/create_enquiry.php",
		'proposal_config': "model/proposal_config.php",
		'proposal': "model/proposal.php",
		'depth_insert': "model/depth_insert.php",
		'terms_insert': "model/terms_insert.php",
		'assumptions_insert': "model/assumptions_insert.php",
		'deliverable_insert': "model/deliverable_insert.php",
		'site_visit_insert': "model/site_visit_insert.php",
		'scope_depth_mapping': "model/scope_depth_mapping.php",
        'manual_notify_insert':"model/manual_notify_insert.php",
        'manual_notify_onload':"model/manual_notify_onload.php",
        'enquiry_track_details':"model/enquiry_track_details.php",
		'testimonial_insert': "model/testimonial_insert.php",
		'enquiry_insert': "model/enquiry_insert.php",
		'proposal_space_mapping': "model/proposal_space_mapping.php",
		'section_insert': "model/section_insert.php",
		'faq_details': "model/faq_details.php",
		'enquiry_email_check': "model/enquiry_email_check.php",
 };
		return pages[page_id];
}

var gsession = {};

gsession.setSession = (function(key, value) {
    if (value) {
        localStorage.setItem(key, value);
    }
});

gsession.getSession = (function(key) {
    return localStorage.getItem(key);
});

gsession.clearSession = (function(key) {
    localStorage.removeItem(key);
});


$(document).on('click', '#logout', function() {
    gsession.clearSession("user_hash");
    var url = window.location.href;
    var n = url.search("app.designqube");
    var demo = url.search("/dqapp");
    var demo_check = url.search("/demo");

    if (n !== -1) {
        document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://app.designqube.com/index.html";
    }

    if (demo_check !== -1) {
        document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://designqube.com/demo/index.html";
    }

    if (demo !== -1) {
        document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://localhost/designqube.com/dqapp/index.html";
    }


});

$(document).ready(function() {

	var url = window.location.href;
    var n = url.search("designqube.com/dqapp");

//	 if (n !== -1) {
//
//		 var local = url.search("localhost");
//
//		 if(local !== -1){
//			 // document.location.href = "http://localhost/designqube.com/dqapp";
//		 }else{
//			  document.location.href = "http://app.designqube.com";
//		 }
//
//      }


    var role = gsession.getSession("role_type");
    var page_name = $("body").attr("data-page_name");

  //  if (page_name != "index") {
  //      var user_hash = gsession.getSession("user_hash");
//        if (!user_hash) {
//            document.location.href = "index.html";
//        }
  //  }

    $(".profile-wrap").css("text-align", "center");
    var profile_pic = gsession.getSession("profile_pic");
    if (profile_pic=="pic") {
        $(".pad-btm .img-circle").attr("src", "img/profile-photos/profile_icon.png");
    }else{
        $(".pad-btm .img-circle").attr("src", profile_pic);
    }
    if (role == "user") {
        $(".dashboard").remove();
        $(".support_ticket").remove();
       
    } else if (role == "developer") {
        $(".dashboard").remove();
        $(".view_expected_funds").remove();
        $(".apply_expected_funds").remove();
    } else if (role == "accounts_admin") {
        $(".dashboard").remove();
        $(".apply_expected_funds").remove();

    } else if (role == "team_head") {
        $(".dashboard").remove();
        $(".support_ticket").remove();

    } else if (role == "super_admin") {
        $(".dashboard").remove();
        $(".apply_expected_funds").remove();
    } else if (role == "hr_admin") {
        $(".view_expected_funds").remove();
        $(".apply_expected_funds").remove();
    }
	
	if(role == "user"){
		
		var multi_roles = gsession.getSession("multi_roles");
		var role_array  = multi_roles.split(",");
		
		if($.inArray("payment_request",role_array) === -1){
			$(".apply_expected_funds").remove();
			$(".view_expected_funds").remove();
		}
	}
	
    var display_name = gsession.getSession("display_name");
    if (role == "hr_admin") {
        var team_name = 'HR Team';
    } else if (role == "accounts_admin") {
        var team_name = "Accounts Team";
    } else if (role == "developer") {
        var team_name = "IT Team";
    } else {
        var team_name = gsession.getSession("team_name");
    }

	var display_data='';

	if (role == "team_head") {
		display_data=display_name+"<br>"+"("+"Team Head"+")"+ "<br>"+ team_name;
	}else{
		display_data=display_name + "<br>" + team_name;
	}
    $(".display_name").html(display_name);
    $('.display_name_dept').html(display_data);
    $(".box-block").removeAttr("data-toggle");
    $(".box-block").removeAttr("aria-expanded");


    if (page_name== "internal_directory") {
      $(".box-block").attr("href", "../update_profile.html");
      $("#mainnav-menu").prepend('<li class = "user_dashboard"><a href="../dashboard.html"> <i class="glyphicon glyphicon-home"></i><span class="menu-title"><strong>Dashboard</strong></span></a></li>');
	  if(role == "team_head" || role == "super_admin"){

		 $("#mainnav-menu").prepend('<li class = "enquiries"><a href="../all_enquiries.html"> <i class="glyphicon glyphicon-bullhorn"></i><span class="menu-title"><strong>Enquiries</strong></span></a></li>');
	  }
      $('a.navbar-brand').attr('href', "../dashboard.html");
    }else{
      $(".box-block").attr("href", "update_profile.html");
      $("#mainnav-menu").prepend('<li class = "user_dashboard"><a href="dashboard.html"> <i class="glyphicon glyphicon-home"></i><span class="menu-title"><strong>Dashboard</strong></span></a></li>');
	  if(role == "team_head" || role == "super_admin"){

		 $("#mainnav-menu").prepend('<li class = "enquiries"><a href="../all_enquiries.html"> <i class="glyphicon glyphicon-bullhorn"></i><span class="menu-title"><strong>Enquiries</strong></span></a></li>');
	  }
	  $('a.navbar-brand').attr('href', "dashboard.html");
    }



});