<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include('header.php');
$result = json_decode($_POST['myData']);
$con_obj=new dbcon();   
$connect_ref=$con_obj->connect(); 

	$action = $result->action;
	$response = array();
	
		  $sketch_object = new sketch_class();
        if($action=="insert_sketch"){
            $response = $sketch_object->insert_sketch($result,$connect_ref);
        }
        else if($action=="update_sketch"){
            $response = $sketch_object->update_sketch($result,$connect_ref);
        }
        else if($action=="select_sketch"){
            $response = $sketch_object->select_sketch($result,$connect_ref);
        }
        else if($action=="fetch_sketch"){
            $response = $sketch_object->fetch_sketch($result,$connect_ref);
        }
        else if($action=="rename_sketch"){
            $response = $sketch_object->rename_sketch($result,$connect_ref);
        }
       
		
		echo json_encode($response);
		
	
	
        //======================================================Sketch_Class============================================================================//


    class sketch_class {
        
        //===========================================Insert Sketches into the Database==================================================================//
        
        function insert_sketch($result,$connect_ref){
             $jsondata = $result->sketch_data;
             $user_hash = $result->user_hash;
             $sketch_name = $result->name;
             $status = "show";
             $timestamp = time();
             $modified_time = $timestamp;
             date_default_timezone_set("Asia/Calcutta");
             $time = date("h:i:sa");
             $response = array();
             $sql1 = "select MAX(sno) from sketch";
             $stmt1 = $connect_ref -> prepare($sql1);
             if($stmt1) {
   
             $stmt1 -> execute();
             $stmt1 -> bind_result($max_id);
             $stmt1 -> fetch();
             $stmt1 -> close();
             
             }
             $max_id++;
             $dataid="sketch"."-".$max_id;
             $sql = "INSERT INTO sketch (`data_id`,`sketch_name`, `user_hash`, `json_data`, `display_status`, `timestamp`,`last_modified_time`,`time`) values(?,?,?,?,?,?,?,?)";
   
             $stmt = $connect_ref -> prepare($sql);
             if($stmt) {
    
             $stmt -> bind_param("ssssssss",$dataid,$sketch_name,$user_hash,$jsondata,$status,$timestamp,$modified_time,$time);
             $stmt -> execute();
             $response['data'] = $dataid;
             $stmt -> close();
                 
             }
             else{
             $response['data'] = 'error';
             }
             return $response;
            } 
        
        
        //============================================Update Sketches into the database===================================================================//
        
         
        
        function update_sketch($result,$connect_ref)
        {
        
             $data_id=$result->active_sketch;
             $jsondata=$result->sketch_data; 
             $modified_time = time();
             date_default_timezone_set("Asia/Calcutta");
             $time = date("h:i:sa");
             $sql="update sketch set json_data=?,last_modified_time=?,time=? where data_id=?";
             $stmt = $connect_ref -> prepare($sql);
             if($stmt) {
             
             $stmt -> bind_param("ssss",$jsondata,$modified_time,$time,$data_id);
             
             $stmt -> execute();
                
             $stmt -> close();
                 
             $response = $jsondata;
             }
             else{
             $response="error";
             }
            return $response;
         } 
        
        
        
        //===========================================Select Sketches from the database=====================================================================//
        
        
        
        
        function select_sketch($result,$connect_ref)
        {
            
            $user_hash = $result->user_hash;
            $sql="select sketch_name,data_id,json_data,last_modified_time,time from sketch where display_status='show' AND user_hash=? ORDER BY last_modified_time  DESC";
            $stmt = $connect_ref -> prepare($sql);
            $response = array();
            $flag=0;
            if($stmt) {
            $stmt -> bind_param("s",$user_hash);
            $stmt -> execute();
            $stmt -> bind_result($sketch_name,$data_id,$json_data,$timestamp,$time);
            while($stmt -> fetch()){
            $response[$flag]['data_id']=$data_id;
            $response[$flag]['name']=$sketch_name;
            $response[$flag]['timestamp']=$this->get_timestamp_by_days_format($timestamp);
            $response[$flag]['time']=$time;
            $flag++;
            }
            $stmt -> close();
            }
            else{
            echo "error";
            }
            return $response;
        }
        
        
        
        
        
        //=========================================Fetch Sketches from the database========================================================================//
        
        
        
        function fetch_sketch($result,$connect_ref)
        {
            $data_id=$result->data_id;
            $sql="select json_data,sketch_name from sketch where data_id=?";
            $stmt = $connect_ref->prepare($sql);
            $response = array();
            if($stmt)
            {
            $stmt->bind_param("s",$data_id);
            $stmt->execute();
            $stmt->bind_result($data,$sketch_name);
            $stmt->fetch();
            $stmt->close();
            }
            else{
            echo "error";
            }
            $response['sketch_data']=$data;
            $response['name']=$sketch_name;
            return $response;
            
        }
        
        
        
        
        //==============================================Rename sketches====================================================================================//
        
        
        function rename_sketch($result,$connect_ref)
        {
            $data_id=$result->active_sketch;
            $sketch_name=$result->name;
            $sql="update sketch set  sketch_name=? where data_id=?";
            $stmt = $connect_ref -> prepare($sql);
            $response = array();
            if($stmt) {
            $stmt -> bind_param("ss",$sketch_name,$data_id);
            $stmt -> execute();
            $stmt -> close();
            $response['data']=$data_id;
            }
            else{
            $response['data']="error";
            }
            return $response;
            
        }
        
        
        
        
        //===================================================Date format======================================================================//
        
        
        
        
        
        function get_timestamp_by_days_format($timestamp){
                
                $time_ago       = $timestamp;
                $cur_time       = time();
                $time_elapsed   = $cur_time - $time_ago;
                $seconds        = $time_elapsed ;
                $minutes        = round($time_elapsed / 60 );
                $hours          = round($time_elapsed / 3600);
                $days           = round($time_elapsed / 86400 );
                $weeks          = round($time_elapsed / 604800);
                $months         = round($time_elapsed / 2600640 );
                $years          = round($time_elapsed / 31207680 );
                
                
                
                // hours calculation
                if($seconds <= 60){
                    $hour = "few seconds ago";
                }elseif($minutes <= 60){
                    $hour = "few minutes ago";
                }elseif($hours <= 24){
                    if($hours == 1){
                        $hour = "an hour ago";
                    }elseif($hours != 0){
                        $hour = "$hours hrs ago";
                    }
                }else{
                        $hour = '0';
                }
    
                //Days calculation
                if($days <= 31){
                    if($days==1){
                        $day = "yesterday";
                    }elseif($days > 0){
                        $day =  "$days days ago";
                    }else{
                        $day = '0';
                    }
                }else{
                        $day = '0';
                }
                
                //Months calculation
                if($months <=12){
                    if($months==1){
                        $month =  "a month ago";
                    }elseif($months != 0){
                        $month =  "$months months ago";
                    }else{
                        $month = '0';
                    }
                }else{
                        $month = '0';
                }
                
                //Years calculation
                if($years==1){
                    $year = "one year ago";
                }elseif($years>1){
                    $year = "$years years ago";
                }else{
                    $year = '0';
                }
                
                if($year != '0'  && $month != '0' && $day != '0'){ // year,month and day check
                    
                    $date_format = $year.$month.$day;
                    $date        = str_replace("ago", " ",$date_format);
                    $date_format = $date."ago";

                }elseif($year != '0' && $month != '0'){ // year and month check

                    $date_format = $year.$month;
                    $date        = str_replace("ago", " ",$date_format);
                    $date_format = $date."ago";

                }elseif($year != '0' && $day != '0'){ //year and day check

                    $date_format = $year.$day;
                    $date        = str_replace("ago", " ",$date_format);
                    $date_format = $date."ago";

                }elseif($year != '0'){ // year check

                    $date_format = $year;

                }elseif($month != '0' && $day != '0'){  // month and day check
                    
                    $date_format = $month.$day;
                    $date        = str_replace("ago", " ",$date_format);
                    $date_format = $date."ago";

                }elseif($month != '0'){ // month check
                    
                    $date_format = $month;

                }elseif($day != '0'){ // day check

                    $date_format = $day;

                }elseif($hour != '0'){ //hour check (include few minutes ago)

                    $date_format = $hour;
                    
                }
                
               // $response[$flag]["date_format"] = $date_format;
                
                return $date_format;
            }
        
    }




























?>